local template = import '../templates/application.jsonnet';
template.generate(
    hostname='efg-main',
    name='main',
    revision='main',
    imageTag='main-f19e64df-snapshot'
)
